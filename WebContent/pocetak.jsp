<%@ page language="java" 
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="tvz.wauj.rgobo.ResultObject" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>vjezba7</title>
		<link rel="stylesheet" type="text/css" href="css.css">
		
		<!-- jquery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		
		<!-- datatables -->
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
		<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
	</head>
<body>
	<fmt:bundle basename="tvz.wauj.rgobo.locale.AppRes">
	<%
		if (request.isUserInRole("administrator") || request.isUserInRole("korisnik"))
		{	
	%>
	
		<form action="Vjezba4" method="POST" name="langForm">
			<fmt:message key="pocetakPage.jezik"/> 
			<select onchange="this.form.submit()" name="languageSelect">
				<option value="en_US"><fmt:message key="pocetakPage.engleski"/></option>
				<option value="hr_HR"i><fmt:message key="pocetakPage.hrvatski"/></option>
			</select>
		</form>
	
		<form action="Vjezba4" method="POST" name="forma">
			<fieldset>
				<fmt:message key="pocetakPage.idzupanije"/>: <input type="text" name="textID"/> <br>
				<fmt:message key="pocetakPage.dohvati" var="varb"/>
				<input type="submit" value="${varb}" name="pocetakSubmit"/>
				<fmt:message key="pocetakPage.dohvatiSve" var="varc"/>
				&emsp;<input type="submit" value="${varc}" name="pocetakSubmitAll"/>
				 <br>
			</fieldset>
		</form>
		
		<%
			if (request.isUserInRole("administrator"))
			{
				out.println("<br/><a href=\"/WauJ-Priprema4-JSTLiDB/update.jsp\">");
				%><fmt:message key="pocetakPage.unesiNovo"/><%
				out.println("</a><br/>");
			}
		%>
	
		<c:choose>
			<c:when test="${errorPocetak == null}">
				<c:choose>
					<c:when test="${queryResult != null}" >
						<br/>
						<table id="myTable">
							<thead>
								<tr>
									<th><fmt:message key="pocetakPage.idzupanije"/></th>
									<th><fmt:message key="pocetakPage.nazivzup"/></th>
									<th><fmt:message key="pocetakPage.brojmjesta"/></th>
								</tr>							
							</thead>
							<tbody>							
								<tr>
									<td><c:out value="${queryResult.getId()}" escapeXml="false"/></td>
									<td><c:out value="${queryResult.getNaziv()}" escapeXml="false"/></td>
									<td><c:out value="${queryResult.getBroj()}" escapeXml="false"/></td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${queryAllResult != null }" >
						<br/>
						<table id="myTable">
							<thead>
								<tr>
									<th><fmt:message key="pocetakPage.idzupanije"/></th>
									<th><fmt:message key="pocetakPage.nazivzup"/></th>
									<th><fmt:message key="pocetakPage.brojmjesta"/></th>
								</tr>							
							</thead>
							<tbody>
								<c:forEach items="${queryAllResult}" var="current">
									<tr>
										<td><c:out value="${current.getId()}" escapeXml="false"/></td>
										<td><c:out value="${current.getNaziv()}" escapeXml="false"/></td>
										<td><c:out value="${current.getBroj()}" escapeXml="false"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<fmt:message key="pocetakPage.notFound"/>
					</c:otherwise>
				</c:choose>				
			</c:when>
			<c:otherwise>
				<c:out value="${errorPocetak}" />
			</c:otherwise>
		</c:choose>
	
	<%
		}
		else 
		{		
			%>
			<fmt:message key="pocetakPage.nedovoljnaprava"/>
			<c:out value="${'<br/><br/>'}"/>
			<c:out value="${'<a href=\"/WauJ-Priprema4-JSTLiDB/login.jsp\">'}" />
			<fmt:message key="loginPage.login"/>
			<c:out value="${'</a><br/>'}"/>
			 <%
		}
	%>
	
	<script>
	$(document).ready(function(){
	    $('#myTable').DataTable();
	});
	

	$('#myTable').DataTable( {
		paging: true
	});
	
	$('#myTable tr:odd').css ( "background-color", "#bbbff" );
	</script>
	
	</fmt:bundle>
</body>
</html>