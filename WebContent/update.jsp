<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update</title>
</head>
<body>

	<% if (request.isUserInRole("administrator"))
		{ 
	%>

	<form action="Vjezba4" method="POST" name="forma">
		<fieldset>
			ID_MJESTA: <input type="text" name="updateID"/> <br/>
			MJESTO: <input type="text" name="updateMjesto"/> <br/>
			HP: <input type="text" name="updateHP"/> <br/>
			POSTURED: <input type="text" name="updatePostured"/> <br/>
			ID_ZUPANIJE: <input type="text" name="updateIDZupanije"/> <br/>
			<input type="submit" value="Unos" name="updateButton"/> <br/>
		</fieldset>
	</form>
	
	<br/><a href="pocetak.jsp">Povratak</a><br/>
	
	<c:choose>
		<c:when test="${errorUpdate == null}">
			<c:out value="${result}" />
		</c:when>
		<c:otherwise>
			<c:out value="${errorUpdate}" />
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${not result == null}">
			<c:out value="${result}" />
		</c:when>
	</c:choose>
	
	<% 
	} else {
		out.println("<br/>Nemate dovoljna prava za pristup ovoj stranici!<br/>");
		out.println("<a href=\"/WauJ-Priprema4-JSTLiDB/login.jsp\"> Login</a><br/>");
		out.println("<a href=\"/WauJ-Priprema4-JSTLiDB/pocetak.jsp\"> Povratak</a><br/>");
	}
	%>
	
</body>
</html>