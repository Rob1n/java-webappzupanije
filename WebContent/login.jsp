<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
</head>
<body>
	<fmt:bundle basename="tvz.wauj.rgobo.locale.AppRes">
	<form action="j_security_check" method="POST" name="forma">
		<fieldset>
			<fmt:message key="loginPage.username"/> <input type="text" name="j_username"/> <br/>
			<fmt:message key="loginPage.password"/> <input type="password" name="j_password"/> <br/>
			<fmt:message key="loginPage.login" var="varb" />
			<input type="submit" value="${varb}" name="loginSubmit"/> <br/>
		</fieldset>
	</form>
	</fmt:bundle>
	
</body>
</html>