package tvz.wauj.rgobo;

public class ResultObject 
{
	private int id;
	private String naziv;
	private int broj;
	
	public ResultObject(int id, String naziv, int broj) 
	{
		super();
		this.id = id;
		this.naziv = naziv;
		this.broj = broj;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getBroj() {
		return broj;
	}
	public void setBroj(int broj) {
		this.broj = broj;
	}
	
	
	
}
