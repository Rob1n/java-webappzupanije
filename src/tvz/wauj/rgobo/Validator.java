package tvz.wauj.rgobo;

public class Validator 
{
	public Validator(){
	}
	
	public String validateIDzupanije(String id)
	{
		if (id == null) { 
			return "ID je null"; 
		}
		if (id == "") { 
			return "ID mora biti zadan!"; 
		}
		if (isNumeric(id)){
			int b = Integer.parseInt(id);
			if (b < 1 || b > 22){
				return "ID županije mora biti između 1 i 22!";
			}
		} else {
			return "ID mora biti broj!";
		}

		return null;
	}

	public String validateLogin(String username, String password)
	{
		if (username == null) return "Username je null";
		if (password == null) return "Password je null";
		if (username == "") return "Korisničko ime mora biti uneseno";
		if (password == "") return "Password mora biti unesen";		
		return null;
	}
	
	public String validateUpdate(String updateID, String updateMjesto, String updateHP, String updatePostured, String updateIDZupanije)
	{
		if (updateID == null) return "updateID je null";
		if (updateMjesto == null) return "updateMjesto je null";
		if (updateHP == null) return "updateHP je null";
		if (updatePostured == null) return "updatePostured je null";
		if (updateIDZupanije == null) return "updateIDZupanije je null";
		
		if (updateID == "") return "ID_MJESTA ne smije biti prazno!";
		if (updateMjesto == "") return "MJESTO ne smije biti prazno!";
		if (updateHP == "") return "HP ne smije biti prazno!";
		if (updatePostured == "") return "POSTURED ne smije biti prazno!";
		if (updateIDZupanije == "") return "IDZupanije ne smije biti prazno!";
		
		if (!isNumeric(updateID)) return "ID mjesta mora biti broj!";
		if (!isNumeric(updateHP)) return "HP mora biti broj!";
		if (!isNumeric(updateIDZupanije)) return "ID zupanije mora biti broj!";
		
		return null;
	}
	
	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{
			return false;  
		}  
		return true;  
	}
	
}
