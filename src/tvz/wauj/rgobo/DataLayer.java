package tvz.wauj.rgobo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataLayer {

	public DataLayer(){
	}
	
	public ResultObject readDatabase(String zupanijaID) throws SQLException, ClassNotFoundException 
	{
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		connection = createConnection();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(
				"SELECT zupanije.idzupanije, zupanije.nazivzupanije, COUNT(mjesta.id_mjesta) AS brojMjesta "
				+ "FROM zupanije "
				+ "JOIN mjesta ON mjesta.idzupanije = zupanije.idzupanije "
				+ "WHERE zupanije.idzupanije = " + zupanijaID
				);
		
		ResultObject data = null;
		
		while (resultSet.next())
		{
			int id = resultSet.getInt("idzupanije");
			String naziv = resultSet.getString("nazivzupanije");
			int broj = resultSet.getInt("brojMjesta");
			
			data = new ResultObject(id, naziv, broj);
		}
		
		resultSet.close();
		statement.close();
		connection.close();
		return data;
	}
	
	public ArrayList<ResultObject> readWholeDatabase() throws SQLException, ClassNotFoundException{
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		connection = createConnection();
		statement = connection.createStatement();
		resultSet = statement.executeQuery(
				"SELECT zupanije.idzupanije, zupanije.nazivzupanije, COUNT(mjesta.id_mjesta) AS brojMjesta "
				+ "FROM zupanije "
				+ "JOIN mjesta ON mjesta.idzupanije = zupanije.idzupanije "
				+ "GROUP BY zupanije.nazivzupanije"
				);
		
		
		ArrayList<ResultObject> data = new ArrayList<>();
		while (resultSet.next())
		{
			int id = resultSet.getInt("idzupanije");
			String naziv = resultSet.getString("nazivzupanije");
			int broj = resultSet.getInt("brojMjesta");
			
			ResultObject temp = new ResultObject(id, naziv, broj);
			data.add(temp);
		}
		
		resultSet.close();
		statement.close();
		connection.close();
		return data;
	}
	
	public String insertNewEntry(String updateID, String updateMjesto, String updateHP, String updatePostured, String updateIDZupanije)
	{
		try 
		{
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			
			connection = createConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(
					"INSERT INTO mjesta "
					//+ "(ID_MJESTA, MJESTO, HP, POSTURED, IDZUPANIJE) "
					+ "VALUES "
					+ "('" 
					+ updateID +"', '" 
					+ updateMjesto +"', '" 
					+ updateHP + "', '" 
					+ updatePostured + "', '" 
					+ updateIDZupanije + "');"
					);
			
			resultSet.close();
			statement.close();
			connection.close();
			return null;
		} 
		catch (Exception e)
		{
			return e.toString();
		}
	}
	
	private Connection createConnection() throws ClassNotFoundException, SQLException
	{
		Connection connection = null;
		Class.forName("org.mariadb.jdbc.Driver");
		connection = DriverManager
				.getConnection("jdbc:mariadb://localhost/waujzupanije?"
						+ "user=root&password=");
		return connection;
	}
	
}
