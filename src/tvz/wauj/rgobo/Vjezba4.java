package tvz.wauj.rgobo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Servlet implementation class Vjezba4
 */
@WebServlet("/Vjezba4")
public class Vjezba4 extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vjezba4() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		try 
		{
			handleRequest(req, res);
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		//doGet(req, res);
		try 
		{
			handleRequest(req, res);
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}

	private void handleRequest(HttpServletRequest req, HttpServletResponse res) throws IOException, ClassNotFoundException, SQLException, ServletException 
	{
		DataLayer dataLayer = new DataLayer();
		HttpSession session = req.getSession();		
		Validator validator = new Validator();
		String error = null;		
			
		String btnPocetak = req.getParameter("pocetakSubmit");
		String btnPocetakAll = req.getParameter("pocetakSubmitAll");
		String btnLogin = req.getParameter("loginSubmit");
		String btnUpdate = req.getParameter("updateButton");
		
		res.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		res.setHeader("Pragma","no-cache"); //HTTP 1.0
		res.setDateHeader ("Expires", 0); //prevents caching at the proxy server
		
		//Locale.getAvailableLocales();
		//ResourceBundle resourceBundle = ResourceBundle.getBundle("tvz.wauj.rgobo.locale.AppRes", new Locale("en_US"));
		String optionLanguage = req.getParameter("languageSelect");
		if (optionLanguage != null)
		{
			if (optionLanguage == "hr_HR")
			{
				Locale.setDefault(new Locale("hr", "HR"));
				Config.set(session, Config.FMT_LOCALE, new Locale("hr", "HR"));
			}
			else if (optionLanguage == "en_US")
			{
				Locale.setDefault(new Locale("en", "US"));
				Config.set(session, Config.FMT_LOCALE, new Locale("en", "US"));
			}
		}
		
		if (btnPocetak != null) 
		{
			// pretrazivanje u tijeku
			ResultObject data = null;
			String zupanijaID = req.getParameter("textID");			
			
			error = validator.validateIDzupanije(zupanijaID);
			if (error == null)
			{
				data = dataLayer.readDatabase(zupanijaID);
				session.setAttribute("queryResult", data);
			} 
			else 
			{
				session.setAttribute("queryResult", null);
			}
			session.setAttribute("errorPocetak", error);
			req.getRequestDispatcher("/pocetak.jsp").forward(req, res);
		} 
		else if (btnPocetakAll != null) 
		{
			ArrayList<ResultObject> list = null;
			list = dataLayer.readWholeDatabase();
			session.setAttribute("queryAllResult", list);
			req.getRequestDispatcher("/pocetak.jsp").forward(req, res);
		}
		else if (btnLogin != null) 
		{
			// login u tijeku - ovo je beskorisno					
			/*String username = req.getParameter("j_username");
			String password = req.getParameter("j_password");
			
			error = validator.validateLogin(username, password);
			if (error == null){
				req.getRequestDispatcher("/login.jsp").forward(req, res);
			}*/
		} 
		else if (btnUpdate != null) 
		{
			// kliknut update
			String updateID = req.getParameter("updateID");
			String updateMjesto = req.getParameter("updateMjesto");
			String updateHP = req.getParameter("updateHP");
			String updatePostured = req.getParameter("updatePostured");
			String updateIDZupanije = req.getParameter("updateIDZupanije");
			
			error = validator.validateUpdate(updateID, updateMjesto, updateHP, updatePostured, updateIDZupanije);
			if (error == null)
			{
				String result = dataLayer.insertNewEntry(updateID, updateMjesto, updateHP, updatePostured, updateIDZupanije);
				if (result == null)
				{
					// sve ok
					session.setAttribute("result", "Podatak uspješno unesen");
				} 
				else 
				{
					session.setAttribute("result", result);
				}				
			} 
			else 
			{
				session.setAttribute("result", null);
			}
			session.setAttribute("errorUpdate", error);
			req.getRequestDispatcher("/update.jsp").forward(req, res);
		} 
		else 
		{
			//
			session.setAttribute("queryResult", null);
			session.setAttribute("queryAllResult", null);
			session.setAttribute("errorPocetak", null);
			session.setAttribute("errorLogin", null);
			session.setAttribute("errorUpdate", null);
			//req.getRequestDispatcher("/login.jsp").forward(req, res);
			res.sendRedirect("/WauJ-Priprema4-JSTLiDB/pocetak.jsp");
		}
		
		
	}
	
}
